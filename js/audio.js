'use strict';
URL = window.URL || window.webkitURL;
 window.localStorage.setItem("playerId",null);
var idC=0;

var mediaRecorder; 						//stream from getUserMedia()
var recorder; 							//Recorder.js object
var mediaStreamAudioSourceNode; 	

var AudioContext = window.AudioContext || window.webkitAudioContext;
var audioContext;

 $(function(){
    // loader({id:1,path:"save/data/vocal_5faefe86f04ba13-11-2020-21-45-42.weba"});

 });     
 function audioInput(id){
    var img =document.getElementById("pp"+id);
    var audio =document.getElementById("audio"+id);
    var avance =document.getElementById("avance"+id);
    var minutes =document.getElementById("minutes"+id);
    var seconds =document.getElementById("seconds"+id);

    audio.currentTime=(avance.value/100)*audio.duration;
            
    startAudio('input',img,audio,avance,minutes,seconds);
    if(img.src.search("play.svg")!=-1) audio.pause(); else audio.play();
 }
 function playPause(id){
    var img =document.getElementById("pp"+id);
    var audio =document.getElementById("audio"+id);
    var avance =document.getElementById("avance"+id);
    var minutes =document.getElementById("minutes"+id);
    var seconds =document.getElementById("seconds"+id);

    if(img.src.search("play.svg")!=-1){
    checkPlayerId(id,"play");

        img.src="pause.svg";

        audio.play();
        avance.value=((audio.currentTime*100)/audio.duration);

        seconds.innerHTML=formatSeconds(Math.floor((audio.currentTime/60).toFixed()));



        idC=setInterval(()=>startAudio('click',img,audio,avance,minutes,seconds),250);
    }else{
        checkPlayerId(id,"pause");
        clearInterval(idC);
        img.src="play.svg";
        audio.pause();
    }
 }

//Cette fonction permet de demarer l'auio et de l'avancer
function startAudio(option,img,audio,avance,minutes,seconds){
            if(Math.floor(((audio.currentTime*100)/audio.duration))>=100) {
                updateTimer(audio,minutes,seconds);
                setTimeout(()=>{
                 img.src="play.svg";minutes.innerHTML="00";seconds.innerHTML="00"; avance.value=0;
                 clearInterval(idC);
            },500);
            }
                        else {
                           if(option=="click"){
                            avance.value=Math.floor(((audio.currentTime*100)/audio.duration));
                           }else{
                            audio.currentTime=Math.floor(((avance.value/100)*audio.duration));
            
                           }
            
                            if(Math.floor(audio.currentTime.toFixed())<60){
                                    minutes.innerHTML="00";
                            seconds.innerHTML=formatSeconds(Math.floor(audio.currentTime.toFixed()));
                          
                            }else{
                                updateTimer(audio,minutes,seconds);
                           
                            }
                           
                          
            
                        }
}
///Cette fonction permet de mettre à jour les secondes et les minutes DOM
function updateTimer(audio,minutes,seconds){
    minutes.innerHTML="00";
                    var min=Math.floor(audio.currentTime.toFixed());
                    do{
                        if(min-60>=0){
                            min-=60;
                        var n=Number(minutes.innerHTML);
                        n++;
                        minutes.innerHTML=`${n}`;
                        }else break;
                      

                    }while(min>=60);
                seconds.innerHTML=formatSeconds(min);

}
//Formatte les seconds pour qu'ils soient afficher à deux chiffre
function formatSeconds(seconds){
if(seconds<10)return "0"+seconds;
else return `${seconds}`;


}
function checkPlayerId(id,option) {
    var playerId=window.localStorage.getItem("playerId");
    if(playerId==null||playerId==undefined||playerId==""||playerId=="null"||playerId=="undefined") window.localStorage.setItem('playerId',id);
    else{
        var oldId=Number(playerId);
        if(oldId!=id){
         if(option=="play"){
            var img =document.getElementById("pp"+oldId);
            if(img.src.search("play.svg")==-1)$("#pp"+oldId).trigger('click');
         }
           
            window.localStorage.setItem('playerId',id);
            clearInterval(idC);
        }
    }
}


function onprogressReader(media)
{
    //get the buffered ranges data
    var ranges = [];
    for(var i = 0; i < media.buffered.length; i ++)
    {
        ranges.push([
            media.buffered.start(i),
            media.buffered.end(i)
            ]);
    }
    
}

// const errorMsgElement = document.querySelector('span#errorMsg');
let recordedBlobs;
let seconds=0;
let idInterval=-1;
const REMOTE_URL="";
// let mime = 'audio/webm;codecs=opus';
// let mime = 'audio/webm';
// let mime = 'audio/wav';
let mime = 'audio/mpeg-3';


  function handleError(error) {
    console.log('navigator.MediaDevices.getUserMedia error: ', error.message, error.name);
  }
  $(function(){
    ///Annuler audio
    document.querySelector('button#annuler_audio').addEventListener('click',  () => {
        stopRecordingAudio();
    
        seconds=0;
        document.querySelector("span#seconds").innerHTML="00";
        ///hide
      });
      ///Sauvegarder audio
      document.querySelector('button#stop').addEventListener('click',  () => {
        
        stopRecordingAudio();
        var durations=seconds;
        seconds=0;
        document.querySelector("span#seconds").innerHTML="00";
      	recorder.exportWAV(showBlob,mime);

      });
      ///Démarer audio
      document.querySelector('button#record').addEventListener('click',  () => {
        const constraints = {
          audio: true
        };
        // console.log('Using media constraints:', constraints);
         initAudio(constraints);
      }); 
    
  });
   function initAudio(constraints) {
    try {
       
        if (!navigator.mediaDevices || !navigator.mediaDevices.enumerateDevices) {
            alert("Votre navigateur n'est pas à jour ou ne supporte la technogie. Veuillez changer de navigateur");
            return;
          }

            navigator.mediaDevices.getUserMedia(constraints).then((stream)=>{
              handleSuccessAudio(stream);

            }).catch(handleError);
          
    } catch (e) {
        alert("Autorisez l'accès à votre microphone pour envoyer des audios");
    //   console.error('navigator.getUserMedia error:', e);
    //   errorMsgElement.innerHTML = `navigator.getUserMedia error:${e.toString()}`;
    }
  }
  function handleSuccessAudio(stream) {
    // console.log('getUserMedia() got stream:', stream);
 
    // window.stream = stream;
    // const gumAudio = document.querySelector('audio#gum_audio');
    // gumAudio.srcObject = stream;
    startRecordingAudio(stream);
    setTimeout(()=>{
      $("#stop").trigger('click');
    },45000);
  }
  function startRecordingAudio(stream) {
    recordedBlobs = [];
    let options = {mimeType: mime};


// try {
//   if (!'MediaSource' in window)console.log('There is no MediaSource property in window object.');
//   if (!MediaSource.isTypeSupported(mime)) {
//     console.log('Can not play the media. Media of MIME type ' + mime + ' is not supported.');
//     console.log('Media of type ' + mime + ' is not supported.');
//     return;
//     }
// } catch (error) {
//   alert(error);
// }

// try {
//   if (!MediaRecorder.isTypeSupported(options.mimeType)) {
//     console.error(`${options.mimeType} is not supported`);

//   }
// } catch (error) {
//   alert(error);
// }
  
    try {
      // mediaRecorder = new MediaRecorder(stream, options);
      // audioContext = new AudioContext({sampleRate : 3000});
      audioContext = new AudioContext({sampleRate : 128});

      mediaRecorder = stream;
      
      mediaStreamAudioSourceNode = audioContext.createMediaStreamSource(stream);
  
      recorder = new Recorder(mediaStreamAudioSourceNode,{numChannels:2,mimeType: mime})
  
      recorder.record()
  
  
    } catch (e) {
      console.error('Exception while creating Audiocontext:', e);
      return;
    }
  
    // mediaRecorder.onstop = (event) => {
    //   console.log('Recorder stopped: ', event);
    //   console.log('Recorded Blobs: ', recordedBlobs);
    // };
    // mediaRecorder.ondataavailable = handleDataAvailable;

      // mediaRecorder.record();
  
      seconds=0;
      document.querySelector("#seconds").innerHTML=seconds;
      idInterval=setInterval(() => {
        seconds++;
        document.querySelector("#seconds").innerHTML=seconds;

      }, 1000);

    }
// function handleDataAvailable(event) {
  
//     if (event.data && event.data.size > 0) {
//       recordedBlobs.push(event.data);

//     }
//   }
function showBlob(blob) {
            var reader = new FileReader();
            reader.readAsDataURL(blob); 
            reader.onloadend = function() {
              var base64data = reader.result;  
              // console.log(base64data);              
              $.post( "upload.php", {"audio_mssg":base64data}, function( data ) {
                  
                try {
                loader(data);

                } catch (error) {
                  //
                }
                });
          }
          

}
  function stopRecordingAudio() {
    try {

    	recorder.stop();

      mediaRecorder.getAudioTracks()[0].stop();
      clearInterval(idInterval);
    } catch (error) {
        alert(error);
    }

 
  }

function loader(data) {
    var id=data.id;
      
    var html="";
      //  var mediaSource = new MediaSource();
    //    html+='<audio id="audio'+id+'" src=""  controls preload="auto"></audio>';
       html+='<div class="voice-container">';
       html+='<img id="pp'+id+'" onclick="playPause('+id+')" class="player-btn" src="./play.svg" alt="Play pause">';
       html+='<input id="avance'+id+'" oninput="audioInput('+id+')" preload="auto" class="slider" type="range" min="0" max="100" value="0"/>';
       html+='<audio id="audio'+id+'" src="'+data.path+'" preload="auto"></audio>';
       html+='<span class="voice-timer"><span id="minutes'+id+'">0</span>:<span id="seconds'+id+'">00</span></span> ';   
       html+=' </div>';
           $("main").append(html);
        var source = document.getElementById("audio"+id);

        source.src = URL.createObjectURL(data.path);
    
        // mediaSource.addEventListener('sourceopen', function() {
        //     var sourceBuffer = this.addSourceBuffer(mime);
        //     // sourceBuffer.appendWindowEnd = 4.0;
        //     var xhr = new XMLHttpRequest;
        //     xhr.open('GET', ''+data.path);
        //     // xhr.open('GET', 'sample.mp3'); /* while testing in localhost */
        //     xhr.responseType = 'arraybuffer';
        //     xhr.onload = function() {
        //         try {
        //             switch (this.status) {
        //                 case 200:
        //                     sourceBuffer.appendBuffer(this.response);
        //                     sourceBuffer.addEventListener('updateend', function (_) {
        //                         mediaSource.endOfStream();
        //                     });
        //                     break;
        //                 case 404:
        //                     throw 'File Not Found';
        //                 default:
        //                     throw 'Failed to fetch the file';
        //             }
        //         } catch (e) {
        //             console.error(e);
        //         }
        //     };
        //     xhr.send();
        // });
}

   
// mediaSource.addEventListener('sourceopen', function() {
//   var sourceBuffer = this.addSourceBuffer(mime);
//   var xhr = new XMLHttpRequest;
//   xhr.open('GET', 'sample.mp3');
//   xhr.responseType = 'arraybuffer';
//   xhr.onload = function() {
//     try {
//       switch (this.status) {
//         case 200:
//             sourceBuffer.appendBuffer(this.response);
//             sourceBuffer.addEventListener('updateend', function (_){
//                 mediaSource.endOfStream();
//             });
//             break;
//         case 404:
//             throw 'File Not Found';
//         default:
//             throw 'Failed to fetch the file';
//       }
//     } catch (e) {
//       console.error(e);
//     }
//   };
//   xhr.send();
// });