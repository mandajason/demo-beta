/*
*  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
*
*  Use of this source code is governed by a BSD-style license
*  that can be found in the LICENSE file in the root of the source
*  tree.
*/

// This code is adapted from
// https://rawgit.com/Miguelao/demos/master/mediarecorder.html

'use strict';

/* globals MediaRecorder */

let mediaRecorder;
let recordedBlobs;
let seconds=0;
const errorMsgElement = document.querySelector('span#errorMsg');
const recordedCamera = document.querySelector('video#recorded');
const recordedAudio = document.querySelector('audio#recorded_audio');
const recordButtonCamera = document.querySelector('button#record');
const recordButtonAudio = document.querySelector('button#record_audio');
recordButtonCamera.addEventListener('click', () => {
  if (recordButtonCamera.textContent === 'Start Recording') {
    startRecording();
  } else {
    stopRecording();
    recordButtonCamera.textContent = 'Start Recording';
    playButtonCamera.disabled = false;
    downloadButtonCamera.disabled = false;
  }
});
recordButtonAudio.addEventListener('click', () => {
  if (recordButtonAudio.textContent === 'Start Recording Audio') {
    startRecordingAudio();
  } else {
    stopRecordingAudio();
    recordButtonAudio.textContent = 'Start Recording Audio';
    playButtonAudio.disabled = false;
    downloadButtonAudio.disabled = false;
  }
});

const playButtonCamera = document.querySelector('button#play');
playButtonCamera.addEventListener('click', () => {
  const superBuffer = new Blob(recordedBlobs, {type: 'video/webm'});
  recordedCamera.src = null;
  recordedCamera.srcObject = null;
  recordedCamera.src = window.URL.createObjectURL(superBuffer);
  recordedCamera.controls = true;
  recordedCamera.play();
});
const playButtonAudio = document.querySelector('button#play_audio');
playButtonAudio.addEventListener('click', () => {
  const superBuffer = new Blob(recordedBlobs, {type: 'audio/webm'});
  recordedAudio.src = null;
  recordedAudio.srcObject = null;
  recordedAudio.src = window.URL.createObjectURL(superBuffer);
  recordedAudio.controls = true;
  recordedAudio.play();
});

const downloadButtonCamera = document.querySelector('button#download');
downloadButtonCamera.addEventListener('click', () => {
  const blob = new Blob(recordedBlobs, {type: 'video/webm'});
  const url = window.URL.createObjectURL(blob);
  const a = document.createElement('a');
  a.style.display = 'none';
  a.href = url;
  a.download = 'test.webm';
  document.body.appendChild(a);
  a.click();
  setTimeout(() => {
    document.body.removeChild(a);
    window.URL.revokeObjectURL(url);
  }, 100);
});
const downloadButtonAudio = document.querySelector('button#download_audio');
downloadButtonAudio.addEventListener('click', () => {
  const blob = new Blob(recordedBlobs, {type: 'audio/opus'});
  const url = window.URL.createObjectURL(blob);
  const a = document.createElement('a');
  a.style.display = 'none';
  a.href = url;
  a.download = 'test.mp3';
  document.body.appendChild(a);
  a.click();
  setTimeout(() => {
    document.body.removeChild(a);
    window.URL.revokeObjectURL(url);
  }, 100);
});

function handleDataAvailable(event) {
  console.log('handleDataAvailable', event);
  if (event.data && event.data.size > 0) {
    recordedBlobs.push(event.data);
  }
}

function startRecording() {
  recordedBlobs = [];
  let options = {mimeType: 'video/webm;codecs=vp9,opus'};
  if (!MediaRecorder.isTypeSupported(options.mimeType)) {
    console.error(`${options.mimeType} is not supported`);
    options = {mimeType: 'video/webm;codecs=vp8,opus'};
    if (!MediaRecorder.isTypeSupported(options.mimeType)) {
      console.error(`${options.mimeType} is not supported`);
      options = {mimeType: 'video/webm'};
      if (!MediaRecorder.isTypeSupported(options.mimeType)) {
        console.error(`${options.mimeType} is not supported`);
        options = {mimeType: ''};
      }
    }
  }

  try {
    mediaRecorder = new MediaRecorder(window.stream, options);
  } catch (e) {
    console.error('Exception while creating MediaRecorder:', e);
    errorMsgElement.innerHTML = `Exception while creating MediaRecorder: ${JSON.stringify(e)}`;
    return;
  }

  console.log('Created MediaRecorder', mediaRecorder, 'with options', options);
  recordButtonCamera.textContent = 'Stop Recording';
  playButtonCamera.disabled = true;
  downloadButtonCamera.disabled = true;
  mediaRecorder.onstop = (event) => {
    console.log('Recorder stopped: ', event);
    console.log('Recorded Blobs: ', recordedBlobs);
  };
  mediaRecorder.ondataavailable = handleDataAvailable;
  mediaRecorder.start();
  console.log('MediaRecorder started', mediaRecorder);
}

function startRecordingAudio() {
  recordedBlobs = [];
  let options = {mimeType: 'audio/wav'};
  if (!MediaRecorder.isTypeSupported(options.mimeType)) {
    console.error(`${options.mimeType} is not supported`);
     options = {mimeType: 'audio/ogg;codecs=opus'};
    if (!MediaRecorder.isTypeSupported(options.mimeType)) {
      console.error(`${options.mimeType} is not supported`);
      options = {mimeType: 'audio/webm;codecs=opus'};
if (!MediaRecorder.isTypeSupported(options.mimeType)) {
  console.error(`${options.mimeType} is not supported`);
  options = {mimeType: 'audio/ogg'};
  if (!MediaRecorder.isTypeSupported(options.mimeType)) {
    console.error(`${options.mimeType} is not supported`);
    options = {mimeType: 'audio/opus'};
    if (!MediaRecorder.isTypeSupported(options.mimeType)) {
      console.error(`${options.mimeType} is not supported`);
      options = {mimeType: ''};
    }
  }
}
      
    }
  }

  try {
    mediaRecorder = new MediaRecorder(window.stream, options);
  } catch (e) {
    console.error('Exception while creating MediaRecorder:', e);
    errorMsgElement.innerHTML = `Exception while creating MediaRecorder: ${JSON.stringify(e)}`;
    return;
  }

  console.log('Created MediaRecorder', mediaRecorder, 'with options', options);
  recordButtonAudio.textContent = 'Stop Recording Audio';
  playButtonAudio.disabled = true;
  downloadButtonAudio.disabled = true;
  mediaRecorder.onstop = (event) => {
    console.log('Recorder stopped: ', event);
    console.log('Recorded Blobs: ', recordedBlobs);
  };
  mediaRecorder.ondataavailable = handleDataAvailable;
  ;
  setTimeout(()=>{
    mediaRecorder.start();
    seconds=0;
    let idInterval=setInterval(() => {
      seconds++;
      document.querySelector("#seconds").innerHTML=seconds;
      if(seconds==15){
      ///shutdown
      console.log("shutd");
      clearInterval(idInterval);
       
       seconds=0;
       recordButtonAudio.click();
      }
    }, 1000);
  },1000);
  console.log('MediaRecorder started', mediaRecorder);
}


function stopRecording() {
  mediaRecorder.stop();
}
function stopRecordingAudio() {
  mediaRecorder.stop();

}
function handleSuccess(stream) {
  recordButtonCamera.disabled = false;
  console.log('getUserMedia() got stream:', stream);
  window.stream = stream;

  const gumCamera = document.querySelector('video#gum');
  gumCamera.srcObject = stream;
}
function handleSuccessAudio(stream) {
  recordButtonAudio.disabled = false;
  console.log('getUserMedia() got stream:', stream);
  window.stream = stream;

  const gumAudio = document.querySelector('audio#gum_audio');
  gumAudio.srcObject = stream;
}
async function init(constraints) {
  try {
    const stream = await navigator.mediaDevices.getUserMedia(constraints);
    handleSuccess(stream);
  } catch (e) {
    console.error('navigator.getUserMedia error:', e);
    errorMsgElement.innerHTML = `navigator.getUserMedia error:${e.toString()}`;
  }
}
async function initAudio(constraints) {
  try {
    const stream = await navigator.mediaDevices.getUserMedia(constraints);
    handleSuccessAudio(stream);
  } catch (e) {
    console.error('navigator.getUserMedia error:', e);
    errorMsgElement.innerHTML = `navigator.getUserMedia error:${e.toString()}`;
  }
}
document.querySelector('button#start').addEventListener('click', async () => {
  const hasEchoCancellation = document.querySelector('#echoCancellation').checked;
  const constraints = {
    audio: {
      echoCancellation: {exact: hasEchoCancellation}
    },
    video: {
      width: 1280, height: 720
    }
  };
  console.log('Using media constraints:', constraints);
  await init(constraints);
}); 

document.querySelector('button#start_audio').addEventListener('click', async () => {
  const hasEchoCancellation = document.querySelector('#echoCancellation').checked;
  const constraints = {
    audio: {
      echoCancellation: {exact: hasEchoCancellation}
    },
  };
  console.log('Using media constraints:', constraints);
  await initAudio(constraints);
}); 