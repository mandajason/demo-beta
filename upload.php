<?php 
 header("Access-Control-Allow-Origin: *");
 header("Content-type: application/json;charset=utf8");
 $path='save/data/'.uniqid("vocal_").date("d-m-Y-H-i-s").".mp3";
 base64_to_audio($_POST['audio_mssg'], $path);
function base64_to_audio($base64_string, $output_file) {
    // open the output file for writing
    
    $ifp = fopen($output_file, 'wb' ); 


    $data = explode( ',', $base64_string );

    // we could add validation here with ensuring count( $data ) > 1
    fwrite( $ifp, base64_decode( $data[ 1 ] ) );

    // clean up the file resource
    fclose( $ifp ); 

    return $output_file; 
}

echo json_encode(array("id"=>time(),"path"=>$path));